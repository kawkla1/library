package com.plusitsolution.library.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.Positive;

import org.elasticsearch.common.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.plusitsolution.library.domain.BookDomain;
import com.plusitsolution.library.domain.CustomerDomain;
import com.plusitsolution.library.enums.BookType;
import com.plusitsolution.library.enums.Status;
import com.plusitsolution.library.enums.StatusRentBook;
import com.plusitsolution.library.es.BookEntity;
import com.plusitsolution.library.es.CustomerEntity;
import com.plusitsolution.library.es.HistoryEntity;
import com.plusitsolution.library.service.BookService;
import com.plusitsolution.library.service.CustomerService;
import com.plusitsolution.library.service.HistoryService;

@RestController
public class LibraryController {

	@Autowired
	BookService bookService;

	@Autowired
	CustomerService customerService;

	@Autowired
	HistoryService historyService;

	@PutMapping("/addbook")
	public BookEntity addBook(@RequestParam String bookName, @RequestParam BookType bookType) {

		return bookService.addBook(bookName, bookType);
	}

	@PutMapping("/update")
	public BookEntity updateBook(@RequestParam("id") String id,
			@RequestParam(name = "name", required = false) String name, @RequestParam("booktype") BookType bookType,
			@RequestParam("status") Status status, @RequestParam("statusRentBook") StatusRentBook statusRentBook) {
		return bookService.update(id, name, bookType, status, statusRentBook);
	}

	@GetMapping("/showbook")
	public List<BookEntity> getBook() {
		return bookService.showBook();
	}

	@PostMapping("/showbookbystatus")
	public List<BookEntity> getBookByStatus(@RequestParam("Status") Status status) {
		return bookService.showBookByStatus(status);
	}

	@DeleteMapping("/del")
	public void clearBook() {
		bookService.clearBook();
	}

	@DeleteMapping("/delbyid")
	public void clearBook(@RequestParam("id") String id) {
		bookService.delBook(id);
	}

	// -----------------------------------------------------------customer

	@PutMapping("/addcustomer")
	public CustomerEntity addCustomer(@RequestBody CustomerDomain customerDomain) {
		return customerService.addCustomer(customerDomain);
	}

	@GetMapping("/showcustomer")
	public List<CustomerEntity> getCustomer() {
		return customerService.showcustomer();
	}

	@DeleteMapping("/delcustomer")
	public void clearCustomer() {
		customerService.clearCustomer();
	}

	// ------------------------------------------------------------history

	@PutMapping("/rentbook")
	public HistoryEntity rentBook(@RequestParam("bookId") String bookId, @RequestParam("customerId") String customerId)
			throws IllegalArgumentException {

		return historyService.rentBook(bookId, customerId);
	}

	@PutMapping("/returnbook")
	public String rentBook(@RequestParam("historyId") String historyId) {

		return historyService.returnBook(historyId);
	}

	@GetMapping("/showhistory")
	public List<HistoryEntity> getHistory() {
		return historyService.showHistory();
	}
	
	
	@PostMapping("/showhistoryByIdCustomer")
	public List<HistoryEntity> getCustomerHistory(@RequestParam("CustomerId") String customerId) {
		return historyService.customerHistory(customerId);
	}

	@DeleteMapping("/delhistory")
	public void clearHistory() {
		historyService.clearHistory();
	}

//	@GetMapping("/test")
//	public String charge(@RequestParam("historyId") String historyId) {
//		return historyService.charge(historyId);
//	}

}