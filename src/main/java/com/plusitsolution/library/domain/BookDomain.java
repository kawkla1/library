package com.plusitsolution.library.domain;

import com.plusitsolution.library.enums.Status;
import com.plusitsolution.library.enums.StatusRentBook;

public class BookDomain {

	private String bookId;
	private String bookName;

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

}
