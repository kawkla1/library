package com.plusitsolution.library.domain;

public class CustomerDomain {

	private String customerName;
	private String custmerPhoneNumber;
	private String customerAge;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustmerPhoneNumber() {
		return custmerPhoneNumber;
	}

	public void setCustmerPhoneNumber(String custmerPhoneNumber) {
		this.custmerPhoneNumber = custmerPhoneNumber;
	}

	public String getCustomerAge() {
		return customerAge;
	}

	public void setCustomerAge(String customerAge) {
		this.customerAge = customerAge;
	}

}
