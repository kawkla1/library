package com.plusitsolution.library.repo;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.plusitsolution.library.enums.Status;
import com.plusitsolution.library.es.BookEntity;
import com.plusitsolution.library.es.CustomerEntity;

@Repository
public interface CustomerRepo extends ElasticsearchRepository<CustomerEntity, String> {
	
	public List<CustomerEntity> findAll();


}
