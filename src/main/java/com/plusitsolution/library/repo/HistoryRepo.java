package com.plusitsolution.library.repo;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.plusitsolution.library.es.CustomerEntity;
import com.plusitsolution.library.es.HistoryEntity;

@Repository
public interface HistoryRepo extends ElasticsearchRepository<HistoryEntity, String>{
	
	public List<HistoryEntity> findAll();
	public List<HistoryEntity> findByCustomerId(String customerId);
}
