package com.plusitsolution.library.repo;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.plusitsolution.library.enums.Status;
import com.plusitsolution.library.es.BookEntity;

@Repository
public interface BookRepo extends ElasticsearchRepository<BookEntity, String> {
	
	public List<BookEntity> findAll();
	public List<BookEntity> findByStatus(Status status);

}
