package com.plusitsolution.library.es;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.plusitsolution.library.enums.BookType;
import com.plusitsolution.library.enums.Status;
import com.plusitsolution.library.enums.StatusRentBook;

@Document(indexName = "book-index")
public class BookEntity {
	
	@Id
	private String bookId;
	
	@Field(type = FieldType.Keyword)
	private String bookName;
	
	private BookType bookType;
	private Status status;
	private StatusRentBook statusRentBook;

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	
	public BookType getBookType() {
		return bookType;
	}

	public void setBookType(BookType bookType) {
		this.bookType = bookType;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public StatusRentBook getStatusRentBook() {
		return statusRentBook;
	}

	public void setStatusRentBook(StatusRentBook statusRentBook) {
		this.statusRentBook = statusRentBook;
	}
}
