package com.plusitsolution.library.es;

import java.sql.Date;
import java.time.LocalDate;

import org.elasticsearch.search.DocValueFormat.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.plusitsolution.library.enums.StatusRent;
import com.plusitsolution.library.enums.StatusRentBook;

@Document(indexName = "history-index")
public class HistoryEntity {
	
	@Id
	private String historyId;
	
	@Field(type = FieldType.Date,format = DateFormat.date)
	private LocalDate historyDateRent;
	
	@Field(type = FieldType.Date,format = DateFormat.date)
	private LocalDate historyDateReturn;
	
	private String bookId;
	
	private String customerId;
	
	private StatusRent statusRent;

	
	public String getHistoryId() {
		return historyId;
	}

	public void setHistoryId(String historyId) {
		this.historyId = historyId;
	}

	public LocalDate getHistoryDateRent() {
		return historyDateRent;
	}

	public void setHistoryDateRent(LocalDate historyDateRent) {
		this.historyDateRent = historyDateRent;
	}

	public LocalDate getHistoryDateReturn() {
		return historyDateReturn;
	}

	public void setHistoryDateReturn(LocalDate historyDateReturn) {
		this.historyDateReturn = historyDateReturn;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public StatusRent getStatusRent() {
		return statusRent;
	}

	public void setStatusRent(StatusRent statusRent) {
		this.statusRent = statusRent;
	}
	
}
