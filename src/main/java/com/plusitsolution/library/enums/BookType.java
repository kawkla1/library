package com.plusitsolution.library.enums;

public enum BookType {
	HISTORY,CARTOON,FICTION,KNOWLEDGE,COMEDY
}
