package com.plusitsolution.library.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plusitsolution.library.enums.Status;
import com.plusitsolution.library.enums.StatusRent;
import com.plusitsolution.library.enums.StatusRentBook;
import com.plusitsolution.library.es.BookEntity;
import com.plusitsolution.library.es.HistoryEntity;
import com.plusitsolution.library.repo.BookRepo;
import com.plusitsolution.library.repo.HistoryRepo;

@Service
public class HistoryService {

	@Autowired
	private HistoryRepo historyRepo;

	@Autowired
	private BookRepo bookRepo;

	public HistoryEntity rentBook(String bookId, String customerId) throws IllegalArgumentException {

		BookEntity bookEntity = bookRepo.findById(bookId).get();

		if (bookEntity.getStatus() == Status.ACTIVE && bookEntity != null) {
			if (bookEntity.getStatusRentBook() == StatusRentBook.UNRENT) {
				HistoryEntity historyEntity = new HistoryEntity();
				LocalDate date = LocalDate.now();

				bookEntity.setStatusRentBook(StatusRentBook.RENT);
				historyEntity.setHistoryId(genId());
				historyEntity.setBookId(bookId);
				historyEntity.setCustomerId(customerId);
				historyEntity.setHistoryDateRent(date);
				historyEntity.setStatusRent(StatusRent.RENTING);

				bookRepo.save(bookEntity);

				return historyRepo.save(historyEntity);

			} else if (bookEntity.getStatusRentBook() == StatusRentBook.RENT) {
				throw new IllegalArgumentException("This bookrented");
			} else {
				throw new IllegalArgumentException("This book not found");
			}
		} else {
			throw new IllegalArgumentException("This book is inactive");
		}
	}

	public void clearHistory() throws IllegalArgumentException {
		try {
			historyRepo.deleteAll();
		} catch (Exception e) {
			throw new IllegalArgumentException("disconnect database");
		}
	}

	public String genId() {

		return "H" + String.format("%04d", (int) historyRepo.count()+1);
	}

	public List<HistoryEntity> showHistory() {
		return historyRepo.findAll();
	}

	public String returnBook(String historyId) {
		
		HistoryEntity historyEntity = new HistoryEntity();
		BookEntity bookEntity = new BookEntity();
		LocalDate date = LocalDate.now();

		
		try {
			historyEntity = historyRepo.findById(historyId).get();
			bookEntity = bookRepo.findById(historyEntity.getBookId()).get();
		} catch (Exception e) {
			throw new IllegalArgumentException("NO History ID");
		}
		
		bookEntity.setStatusRentBook(StatusRentBook.UNRENT);
		historyEntity.setStatusRent(StatusRent.RENTED);
		historyEntity.setHistoryDateReturn(date);

		bookRepo.save(bookEntity);
		historyRepo.save(historyEntity);
		
		String charge = charge(historyEntity.getHistoryDateRent(), historyEntity.getHistoryDateReturn());

		return charge;
	}

	public String charge(LocalDate dateRent, LocalDate dateReturn) {

		int dayPay = 3, payParDay = 10;

		//LocalDate dateTest = LocalDate.of(2021, 3, 11);

		int dayCheck = (int) ChronoUnit.DAYS.between(dateRent, dateReturn);
		String charge = "no charge";

		if (dayCheck > dayPay) {
			charge = "" + (dayCheck - dayPay) * payParDay;
		}
		
		return charge;
	}

	public List<HistoryEntity> customerHistory(String customerId) {
		
		return historyRepo.findByCustomerId(customerId);
		
	}
}
