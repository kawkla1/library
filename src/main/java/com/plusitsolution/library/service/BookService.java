package com.plusitsolution.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plusitsolution.library.domain.BookDomain;
import com.plusitsolution.library.enums.BookType;
import com.plusitsolution.library.enums.Status;
import com.plusitsolution.library.enums.StatusRentBook;
import com.plusitsolution.library.es.BookEntity;
import com.plusitsolution.library.repo.BookRepo;

@Service
public class BookService {

	@Autowired
	private BookRepo bookRepo;

	public BookEntity addBook(String bookName, BookType bookType) {

		BookEntity bookEntity = new BookEntity();
		bookEntity.setBookId(genId());
		bookEntity.setBookName(bookName);
		bookEntity.setBookType(bookType);
		bookEntity.setStatus(Status.ACTIVE);
		bookEntity.setStatusRentBook(StatusRentBook.UNRENT);

		return bookRepo.save(bookEntity);
	}

	public String genId() {

		return "B" + String.format("%04d", (int) bookRepo.count() + 1);
	}

	public List<BookEntity> showBook() {
		return bookRepo.findAll();
	}

	public List<BookEntity> showBookByStatus(Status status) {
		return bookRepo.findByStatus(status);
	}

	public void clearBook() throws IllegalArgumentException {
		try {
			bookRepo.deleteAll();
		} catch (Exception e) {
			throw new IllegalArgumentException("disconnect database");
		}
	}

	public void delBook(String id) {
		bookRepo.deleteById(id);
	}

//	public BookEntity update(String id, BookDomain bookDomain) {
//		BookEntity bookEntity = bookRepo.findById(id).get();
//		
//		bookEntity.setBookName(bookDomain.getBookName());
//		bookEntity.setStatus(bookDomain.getStatus());
//		bookEntity.setStatusRentBook(bookDomain.getStatusRentBook());
//		
//		return bookRepo.save(bookEntity);
//	}

	public BookEntity update(String id, String name, BookType bookType, Status status, StatusRentBook statusRentBook)
			throws IllegalArgumentException {
		BookEntity bookEntity = new BookEntity();

		try {
			bookEntity = bookRepo.findById(id).get();
		} catch (Exception e) {
			throw new IllegalArgumentException("NO Book ID");
		}

		// if (bookEntity != null) {

		if (name != null) {
			bookEntity.setBookName(name);
		}

		bookEntity.setBookType(bookType);
		bookEntity.setStatus(status);
		bookEntity.setStatusRentBook(statusRentBook);

		return bookRepo.save(bookEntity);
//		} else {
//			throw new IllegalArgumentException("NO ID");
//		}

	}

}
