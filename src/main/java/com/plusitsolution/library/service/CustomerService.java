package com.plusitsolution.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plusitsolution.library.domain.CustomerDomain;
import com.plusitsolution.library.es.CustomerEntity;
import com.plusitsolution.library.repo.CustomerRepo;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepo customerRepo;

	public CustomerEntity addCustomer(CustomerDomain customerDomain) {
		CustomerEntity customerEntity = new CustomerEntity();
		customerEntity.setCustomerId(genId());
		customerEntity.setCustomerName(customerDomain.getCustomerName());
		customerEntity.setCustomerPhoneNumber(customerDomain.getCustmerPhoneNumber());
		customerEntity.setCustomerAge(customerDomain.getCustomerAge());

		return customerRepo.save(customerEntity);
	}

	public String genId() {

		return "C" + String.format("%04d", (int) customerRepo.count() + 1);
	}

	public List<CustomerEntity> showcustomer() {
		return customerRepo.findAll();
	}

	public void clearCustomer() throws IllegalArgumentException {
		try {
			customerRepo.deleteAll();
		} catch (Exception e) {
			throw new IllegalArgumentException("disconnect database");
		}
	}

}
